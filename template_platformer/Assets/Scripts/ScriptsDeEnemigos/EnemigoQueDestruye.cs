using UnityEngine;

public class EnemigoQueDestruye : MonoBehaviour
{
    public string playerTag = "Player";

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(playerTag))
        {
            Destroy(collision.gameObject);
        }
    }
}