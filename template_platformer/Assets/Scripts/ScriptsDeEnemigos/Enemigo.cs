using UnityEngine;

public class Enemigo : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            Vector3 closestPoint = collision.collider.ClosestPointOnBounds(transform.position);
            if (closestPoint.y > transform.position.y)
            {
                Debug.Log("El jugador ha tocado el enemigo desde arriba");
            }
            else
            {
                Debug.Log("El jugador ha tocado el enemigo desde los lados o desde abajo");
                Destroy(collision.gameObject);
            }
        }
    }
}
