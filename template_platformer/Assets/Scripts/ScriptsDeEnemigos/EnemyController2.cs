using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController2 : MonoBehaviour
{
    public float speed = 3.0f; // Velocidad del enemigo
    public float detectDistance = 5.0f; // Distancia de detecci�n del jugador
    public float jumpForce = 5.0f; // Fuerza del salto del enemigo
    public Vector3 direction = Vector3.right; // Direcci�n inicial del enemigo


    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, detectDistance))
        {
            if (hit.collider.CompareTag("Player"))
            {
                if (hit.collider.transform.position.y > transform.position.y)
                {
                    Jump();
                }
                else
                {
                    direction = (hit.collider.transform.position - transform.position).normalized;
                    transform.position += direction * speed * Time.deltaTime;
                }
            }
        }
        else
        {
            transform.position += direction * speed * Time.deltaTime;
        }
    }

    void Jump()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}
