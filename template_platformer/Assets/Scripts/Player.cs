using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 5.0f;  // Velocidad del jugador
    public float jumpForce = 10.0f;  // Fuerza del salto
    public LayerMask groundLayer;  // Capa de la superficie

    private Rigidbody rb;
    private Collider coll;
    private bool isGrounded = true;

    // M�todo que se ejecuta una vez al inicio del juego
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();
    }

    // M�todo que se ejecuta una vez por frame
    void Update()
    {
        // Obtener la direcci�n del movimiento
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // Crear un vector de movimiento
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        // Aplicar la velocidad y el delta time al movimiento
        rb.AddForce(movement * speed);

        // Saltar si el jugador est� en el suelo y presiona la tecla de salto
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }

    // M�todo que se ejecuta cuando el jugador choca con otro objeto
    void OnCollisionEnter(Collision collision)
    {
        // Verificar si el jugador est� tocando el suelo
        if (collision.collider.gameObject.layer == groundLayer)
        {
            isGrounded = true;
        }
    }
}
