using UnityEngine;

public class Controller_Player_Tama�o : MonoBehaviour



{
    public float growthRate = 0.1f; // Tasa de crecimiento
    public float maxScale = 2f; // Tama�o m�ximo que puede alcanzar el personaje

    private void OnCollisionStay(Collision collision)
    {
        // Verificamos si la colisi�n es con otro jugador
        if (collision.gameObject.CompareTag("Player"))
        {
            // Aumentamos la escala del jugador mientras se mantengan en contacto
            collision.gameObject.transform.localScale += Vector3.one * growthRate * Time.deltaTime;
            // Limitamos la escala m�xima
            if (collision.gameObject.transform.localScale.x > maxScale)
            {
                collision.gameObject.transform.localScale = Vector3.one * maxScale;
            }
        }
    }
}

